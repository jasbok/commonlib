/*
 * Copyright (C) 2022 Stefan Alberts
 * This file is part of Toolbox.
 *
 * Toolbox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toolbox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
 */
#include "toolbox/common/event.h"
#include "toolbox/sdl2/event.h"
#include "toolbox/sdl2/gl_app.h"

class sdl_app : tb::gl_app {
	tb::sdl_event_handler event_handler_;
	tb::sdl_mousebutton_handler mousebutton_handler_;
	tb::sdl_key_handler key_handler_;
	tb::sdl_window_handler window_handler_;

	bool running_ = true;

	bool use_relative_mouse_motion_ = true;

	void
	quit_(const SDL_Event& e) {
		running_ = false;
	}

	auto
	toggle_mouse_motion_() {
		auto mm_relative = [](const SDL_Event& e){
			printf("Relative mouse motion: [%i, %i]\n",
			       e.motion.xrel,
			       e.motion.yrel);
		};

		auto mm_window = [](const SDL_Event& e){
			printf("Mouse motion (window location): [%i, %i]\n",
			       e.motion.x,
			       e.motion.y);
		};

		use_relative_mouse_motion_ = !use_relative_mouse_motion_;

		return use_relative_mouse_motion_ ? mm_relative : mm_window;
	}

public:
	sdl_app()
		: event_handler_(),
		  key_handler_()
	{
		init("SDL2 Test App", {640, 480}, 3, 2);
		setup_key_events();
		setup_mousebutton_events();
		setup_mousemotion_events();
		setup_window_events();
		setup_quit_mapping();
	}

	void
	post_init() override {
		printf("Running post initialisation.\n");
	}

	void
	setup_mousemotion_events() {
		auto handle = use_relative_mouse_motion_ ?

		              [] (const SDL_Event& e) {
			printf("Relative mouse motion: [%i, %i]\n",
			       e.motion.xrel,
			       e.motion.yrel);
		}
		                             :
		[] (const SDL_Event& e) {
			printf("Mouse motion: [%i, %i]\n",
			       e.motion.x,
			       e.motion.y);
		};

		event_handler_.insert({SDL_MOUSEMOTION, handle});
	}

	void
	setup_mousebutton_events() {
		auto handle = [this](const SDL_Event& e){
			this->mousebutton_handler_.handle(
				e.button.button,
				e.button);
		};

		event_handler_.insert({SDL_MOUSEBUTTONDOWN, handle});
		// event_handler_.set({SDL_MOUSEBUTTONUP, handle});

		mousebutton_handler_.insert({
			{SDL_BUTTON_LEFT, [](const auto&){ printf("LEFT CLICK...\n");}},
			{SDL_BUTTON_RIGHT, [](const auto&){ printf("RIGHT CLICK...\n");}},
			{SDL_BUTTON_MIDDLE, [](const auto&){ printf("MIDDLE CLICK...\n");}}
		});
	}

	void
	setup_key_events() {
		auto handle = [this](const SDL_Event& e){
			this->key_handler_.handle(e.key.keysym.sym, e.key);
		};

		event_handler_.insert({SDL_KEYDOWN, handle});
		// event_handler_.insert({SDL_KEYUP, handle});

		key_handler_.insert({
			{
				SDLK_m, [this](const auto&){
					this->event_handler_.insert({SDL_MOUSEMOTION, this->toggle_mouse_motion_()});
				}
			},
			{SDLK_q, [this](const auto& _){ this->running_ = false; }},
		});
	}

	void
	setup_window_events() {
		auto handle = [this](const SDL_Event& e){
			this->window_handler_.handle(e.window.event, e.window);
		};

		event_handler_.insert({SDL_WINDOWEVENT, handle});

		window_handler_.insert({
			SDL_WINDOWEVENT_RESIZED, [](const SDL_WindowEvent& e){
				printf(
					"The window has been resized: [%i, %i]\n",
					e.data1,
					e.data2);
			}
		});
	}

	void
	setup_quit_mapping() {
		event_handler_.insert({
			SDL_QUIT, [this](const auto& _){ this->running_ = false; }
		});
	}

	void
	run() override {
		printf("Running SDL test app...\n");
		SDL_Event event;

		while(running_) {
			while(SDL_PollEvent(&event)) {
				event_handler_.handle(event.type, event);
			}

			SDL_Delay(30);
		}
	}
}; // class sdl_app

int
main() {
	auto app = sdl_app();
	app.run();

	return 0;
}
