//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#include "test/tests.h"

#include "toolbox/streams/trie.h"

using namespace tb::streams;

TEST_CASE("tb::streams::trie Create trivial trie."){
	auto strs = std::vector<std::string>{"abc", "def", "ghi"};
	auto trie = trie::from(strs);

	REQUIRE(trie.size() == 10);
	CHECK(trie[0].val == 0);

	REQUIRE(trie[0].children.size() == 3);
	CHECK(trie[0].children[0] == 1);
	CHECK(trie[0].children[1] == 4);
	CHECK(trie[0].children[2] == 7);

	CHECK(trie[1].val == 'a');
	REQUIRE(trie[1].children.size() == 1);
	CHECK(trie[1].children[0] == 2);

	CHECK(trie[2].val == 'b');
	REQUIRE(trie[2].children.size() == 1);
	CHECK(trie[2].children[0] == 3);

	CHECK(trie[3].val == 'c');
	REQUIRE(trie[3].children.size() == 0);


	CHECK(trie[4].val == 'd');
	REQUIRE(trie[4].children.size() == 1);
	CHECK(trie[4].children[0] == 5);

	CHECK(trie[5].val == 'e');
	REQUIRE(trie[5].children.size() == 1);
	CHECK(trie[5].children[0] == 6);

	CHECK(trie[6].val == 'f');
	REQUIRE(trie[6].children.size() == 0);


	CHECK(trie[7].val == 'g');
	REQUIRE(trie[7].children.size() == 1);
	CHECK(trie[7].children[0] == 8);

	CHECK(trie[8].val == 'h');
	REQUIRE(trie[8].children.size() == 1);
	CHECK(trie[8].children[0] == 9);

	CHECK(trie[9].val == 'i');
	REQUIRE(trie[9].children.size() == 0);
}

TEST_CASE("tb::streams::trie Create basic trie."){
	auto strs = std::vector<std::string>{"abc", "ade", "abf", "ghi"};
	auto trie = trie::from(strs);

	REQUIRE(trie.size() == 10);
	CHECK(trie[0].val == 0);

	REQUIRE(trie[0].children.size() == 2);
	CHECK(trie[0].children[0] == 1);
	CHECK(trie[0].children[1] == 7);

	CHECK(trie[1].val == 'a');
	REQUIRE(trie[1].children.size() == 2);
	CHECK(trie[1].children[0] == 2);
	CHECK(trie[1].children[1] == 4);

	CHECK(trie[2].val == 'b');
	REQUIRE(trie[2].children.size() == 2);
	CHECK(trie[2].children[0] == 3);
	CHECK(trie[2].children[1] == 6);

	CHECK(trie[3].val == 'c');
	REQUIRE(trie[3].children.size() == 0);

	CHECK(trie[6].val == 'f');
	REQUIRE(trie[6].children.size() == 0);


	CHECK(trie[4].val == 'd');
	REQUIRE(trie[4].children.size() == 1);
	CHECK(trie[4].children[0] == 5);

	CHECK(trie[5].val == 'e');
	REQUIRE(trie[5].children.size() == 0);

	CHECK(trie[7].val == 'g');
	REQUIRE(trie[7].children.size() == 1);
	CHECK(trie[7].children[0] == 8);

	CHECK(trie[8].val == 'h');
	REQUIRE(trie[8].children.size() == 1);
	CHECK(trie[8].children[0] == 9);

	CHECK(trie[9].val == 'i');
	REQUIRE(trie[9].children.size() == 0);
}

TEST_CASE("tb::streams::trie Traverse Trie."){
	auto strs = std::vector<std::string>{"string", "str", "integer", "int",
		                             "floating point", "float", "double", "vector"};

	auto trie = trie::from(strs);
	REQUIRE(trie.size() == 40);

	CHECK(trie::traverse(trie, "string") == 6);
	CHECK(trie::traverse(trie, "str") == 3);
	CHECK(trie::traverse(trie, "integer") == 13);
	CHECK(trie::traverse(trie, "int") == 9);
	CHECK(trie::traverse(trie, "floating point") == 27);
	CHECK(trie::traverse(trie, "float") == 18);
	CHECK(trie::traverse(trie, "double") == 33);
	CHECK(trie::traverse(trie, "vector") == 39);

	CHECK(trie::traverse(trie, "strin") == 5);
	CHECK(trie::traverse(trie, "st") == 2);
	CHECK(trie::traverse(trie, "intege") == 12);
	CHECK(trie::traverse(trie, "in") == 8);
	CHECK(trie::traverse(trie, "floating poin") == 26);
	CHECK(trie::traverse(trie, "floa") == 17);
	CHECK(trie::traverse(trie, "doubl") == 32);
	CHECK(trie::traverse(trie, "vecto") == 38);

	CHECK(trie::traverse(trie, "") == 0);
	CHECK(trie::traverse(trie, "s") == 1);
	CHECK(trie::traverse(trie, "i") == 7);
	CHECK(trie::traverse(trie, "f") == 14);
	CHECK(trie::traverse(trie, "d") == 28);
	CHECK(trie::traverse(trie, "v") == 34);

	CHECK(trie::traverse(trie, "strings") == 0);
	CHECK(trie::traverse(trie, "strs") == 0);
	CHECK(trie::traverse(trie, "integers") == 0);
	CHECK(trie::traverse(trie, "ints") == 0);
	CHECK(trie::traverse(trie, "floating points") == 0);
	CHECK(trie::traverse(trie, "floats") == 0);
	CHECK(trie::traverse(trie, "doubles") == 0);
	CHECK(trie::traverse(trie, "vectors") == 0);

	CHECK(trie::traverse(trie, "abc") == 0);
	CHECK(trie::traverse(trie, "123") == 0);
}
