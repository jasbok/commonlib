//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#include "test/tests.h"

#include "toolbox/streams/streams.h"


using namespace tb::streams;

TEST_CASE("tb::streams: operator|"){
	auto vec = range(20)
	           | filter([](auto i){ return i%2;})
	           | skip(3)
	           | limit(4)
	           | map([](auto i){ return 2*i; })
	           | to_vec();
	CHECK(vec == std::vector{14, 18, 22, 26});

	auto range_20 = range(20);
	CHECK((range_20 | to_vec())
	      == std::vector<int>{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
	                          11, 12, 13, 14, 15, 16, 17, 18, 19});

	auto odds = range_20 | filter([](auto i){ return i%2;});
	CHECK((odds | to_vec())
	      == std::vector<int>{1, 3, 5, 7, 9, 11, 13, 15, 17, 19});

	auto odds_3 = odds | skip(3);
	CHECK((odds_3 | to_vec())
	      == std::vector<int>{7, 9, 11, 13, 15, 17, 19});

	auto odds_3_4 = odds_3 | limit(4);
	CHECK((odds_3_4 | to_vec())
	      == std::vector<int>{7, 9, 11, 13});

	auto doubled = odds_3_4 | map([](auto i){ return 2*i; });
	CHECK((doubled | to_vec())
	      == std::vector<int>{14, 18, 22, 26});
}

TEST_CASE("tb::streams: fold"){
	auto sum = range(10) | fold(0, [](auto acc, auto i){ return acc + i; });
	CHECK(sum == 45);

	auto odd_sum = range(10)
	               | filter([](auto i){ return i%2;})
	               | fold(0, [](auto acc, auto i){ return acc + i; });
	CHECK(odd_sum == 25);
}

TEST_CASE("tb::streams: zip"){
	using pairs = std::vector<std::pair<int, int>>;
	auto evens = range(10) | filter([](auto i){ return !(i%2);});
	auto odds = range(10) | filter([](auto i){ return i%2;});

	auto evens_odds = evens | zip(odds) | to_vec();
	CHECK(evens_odds == pairs{{0, 1}, {2, 3}, {4, 5}, {6, 7}, {8, 9}});

	auto odds_evens = odds | zip(evens) | to_vec();
	CHECK(odds_evens == pairs{{1, 0}, {3, 2}, {5, 4}, {7, 6}, {9, 8}});

	auto evens_odds_3 = evens | zip(odds| limit(3)) | to_vec();
	CHECK(evens_odds_3 == pairs{{0, 1}, {2, 3}, {4, 5}, {6, 0}, {8, 0}});

	auto odds_3_evens = odds | limit(3) | zip(evens) | to_vec();
	CHECK(odds_3_evens == pairs{{1, 0}, {3, 2}, {5, 4}});

	auto evens_3_odds = evens | limit(3) | zip(odds) | to_vec();
	CHECK(evens_3_odds == pairs{{0, 1}, {2, 3}, {4, 5}});

	auto odds_evens_3 = odds | zip(evens | limit(3)) | to_vec();
	CHECK(odds_evens_3 == pairs{{1, 0}, {3, 2}, {5, 4}, {7, 0}, {9, 0}});
}

TEST_CASE("tb::streams: repeat"){
	CHECK((repeat(1) | limit(5) | to_vec())
	      == std::vector{1, 1, 1, 1, 1,});

	using str = std::string;
	using strs = std::vector<str>;

	CHECK((repeat(str("rep")) | limit(5) | to_vec())
	      == strs{"rep", "rep", "rep", "rep", "rep",});
}

TEST_CASE("tb::streams: cycle"){
	CHECK((cycle(std::vector{1,2,3}) | limit(10) | to_vec())
	      == std::vector{1, 2, 3, 1, 2, 3, 1, 2, 3, 1});
}

TEST_CASE("tb::streams: iterator"){
	const std::vector vec = {5, 3, 6, 9};
	auto copy = iterator(vec);
	REQUIRE(copy.next().value() == 5);
	REQUIRE(copy.next().value() == 3);
	REQUIRE(copy.next().value() == 6);
	REQUIRE(copy.next().value() == 9);
	REQUIRE(copy.next() == std::nullopt);

	auto move = iterator(std::vector{2, 1, 3, 5});
	REQUIRE(move.next().value() == 2);
	REQUIRE(move.next().value() == 1);
	REQUIRE(move.next().value() == 3);
	REQUIRE(move.next().value() == 5);
	REQUIRE(move.next() == std::nullopt);

	auto empty = iterator(std::vector<int>{});
	REQUIRE(empty.next() == std::nullopt);

	auto init = iterator({9, 7, 4, 3});
	REQUIRE(init.next().value() == 9);
	REQUIRE(init.next().value() == 7);
	REQUIRE(init.next().value() == 4);
	REQUIRE(init.next().value() == 3);
	REQUIRE(init.next() == std::nullopt);

	auto char_array = iterator("string");
	REQUIRE(char_array.next().value() == 's');
	REQUIRE(char_array.next().value() == 't');
	REQUIRE(char_array.next().value() == 'r');
	REQUIRE(char_array.next().value() == 'i');
	REQUIRE(char_array.next().value() == 'n');
	REQUIRE(char_array.next().value() == 'g');
	REQUIRE(char_array.next() == std::nullopt);
}

TEST_CASE("tb::streams: concat"){
	auto head = range(0, 5); auto tail = range(5, 10); empty<int> empty;

	CHECK((head | concat(tail) | to_vec())
	      == std::vector{0, 1, 2, 3, 4, 5, 6, 7, 8, 9});

	CHECK((empty | concat(tail) | to_vec())
	      == std::vector{5, 6, 7, 8, 9});

	CHECK((head | concat(empty) | to_vec())
	      == std::vector{0, 1, 2, 3, 4});
}

TEST_CASE("tb::streams: recurrent"){
	auto recur = iterator({1, 2, 3, 4, 5})
	             | recurrent(0, [](auto r, auto it){ return r + it; });

	CHECK((recur | to_vec()) == std::vector<int>{1, 3, 6, 10, 15});
}

TEST_CASE("tb::streams: flatten"){
	auto it2 = iterator({iterator({1, 2}), iterator({3, 4}), iterator({5, 6})});

	REQUIRE((it2.next().value() | to_vec()) == std::vector{1, 2});
	REQUIRE((it2.next().value() | to_vec()) == std::vector{3, 4});
	REQUIRE((it2.next().value() | to_vec()) == std::vector{5, 6});
	CHECK(it2.next() == std::nullopt);

	CHECK((it2 | flatten() | to_vec()) == std::vector{1, 2, 3, 4, 5, 6});
}

TEST_CASE("tb::streams: flat_map"){
	auto it2 = iterator({iterator({1, 2}), iterator({3, 4}), iterator({5, 6})});
	auto fm = it2 | flat_map([](auto i){ return i*2; });
	CHECK((fm | to_vec()) == std::vector{2, 4, 6, 8, 10, 12});
}

TEST_CASE("tb::streams: file"){
	auto path = test_file_path("test.csv");
	auto csv = file(path) | limit(10) | to_vec();
	CHECK(csv == std::vector<char>{'i', 'd', ' ', '|', ' ', 't', 'y', 'p', 'e', ' '});
}

TEST_CASE("tb::streams: head"){
	auto vec = iterator(std::vector{1, 2, 3, 4, 5, 6});
	CHECK((vec | head(-1)) == std::vector<int>{});
	CHECK((vec | head(0)) == std::vector<int>{});
	CHECK((vec | head(1)) == std::vector{1});
	CHECK((vec | head(3)) == std::vector{1, 2, 3});
	CHECK((vec | head(6)) == std::vector{1, 2, 3, 4, 5, 6});
	CHECK((vec | head(10)) == std::vector{1, 2, 3, 4, 5, 6});
}

TEST_CASE("tb::streams: first"){
	auto none = iterator(std::vector<int>{}) | first();
	auto one = iterator(std::vector<int>{3}) | first();
	auto more = iterator(std::vector<int>{3, 6, 9}) | first();

	REQUIRE(none == std::nullopt);
	REQUIRE(one != std::nullopt);
	REQUIRE(more != std::nullopt);
	CHECK(one.value() == 3);
	CHECK(more.value() == 3);
}

TEST_CASE("tb::streams: tail"){
	auto vec = iterator(std::vector{1, 2, 3, 4, 5, 6});
	CHECK((vec | tail(-1)) == std::vector<int>{});
	CHECK((vec | tail(0)) == std::vector<int>{});
	CHECK((vec | tail(1)) == std::vector{6});
	CHECK((vec | tail(3)) == std::vector{4, 5, 6});
	CHECK((vec | tail(6)) == std::vector{1, 2, 3, 4, 5, 6});
	CHECK((vec | tail(10)) == std::vector{1, 2, 3, 4, 5, 6});
}

TEST_CASE("tb::streams: last"){
	auto none = iterator(std::vector<int>{}) | last();
	auto one = iterator(std::vector<int>{5}) | last();
	auto more = iterator(std::vector<int>{5, 9, 13}) | last();

	REQUIRE(none == std::nullopt);
	REQUIRE(one != std::nullopt);
	REQUIRE(more != std::nullopt);
	CHECK(one.value() == 5);
	CHECK(more.value() == 13);
}

TEST_CASE("tb::streams: operator|(iterable)"){
	auto vec = std::vector{1, 2, 3, 4, 5, 6};
	CHECK((vec | to_vec()) == vec);

	auto twice = map([](auto i){ return i * 2; });
	CHECK((vec | twice | to_vec()) == std::vector{2, 4, 6, 8, 10, 12});

	auto str = "123456";
	CHECK((str | to_vec()) == std::vector{'1', '2', '3', '4', '5', '6'});

	auto strs = std::vector{"123", "456", "789"};
	CHECK((strs | to_vec()) == std::vector{"123", "456", "789"});
}

TEST_CASE("tb::streams: fold and recurrent"){
	auto vec = std::vector<int>{1, 2, 3, 4};
	auto fol = vec | fold(1, [](auto r, auto i){ return r + i; });
	auto rec = vec | recurrent(1, [](auto r, auto i){ return r + i; });

	CHECK(fol == 11);
	CHECK((rec | to_vec()) == std::vector{2, 4, 7, 11});

	auto rec_last = rec | last();
	REQUIRE(rec_last.has_value());
	CHECK(rec_last.value() == 11);
}

TEST_CASE("tb::streams: sort"){
	auto vec = std::vector<int>{1, 3, 2, 5, 4, 7, 6, 9, 8};
	auto asc = [](int a, int b){ return a < b; };
	auto desc = [](int a, int b){ return a > b; };
	CHECK((vec | sort(asc)) == std::vector{1, 2, 3, 4, 5, 6, 7, 8, 9});
	CHECK((vec | sort(desc)) == std::vector{9, 8, 7, 6, 5, 4, 3, 2, 1});

	using ic = std::pair<int, char>;
	auto vec2 = std::vector<ic>{
		{9,'a'}, {7,'c'}, {8,'b'}, {5,'e'}, {6,'d'},
		{3,'g'}, {4,'f'}, {1,'i'}, {2,'h'}
	};

	auto asc_int = [](ic a, ic b){ return a.first < b.first; };
	auto desc_int = [](ic a, ic b){ return a.first > b.first; };
	auto asc_char = [](ic a, ic b){ return a.second < b.second; };
	auto desc_char = [](ic a, ic b){ return a.second > b.second; };

	CHECK((vec2 | sort(asc_int)) == std::vector<ic>{
		{1, 'i'}, {2, 'h'}, {3, 'g'}, {4, 'f'}, {5, 'e'},
		{6, 'd'}, {7, 'c'}, {8, 'b'}, {9, 'a'}
	});
	CHECK((vec2 | sort(desc_int)) == std::vector<ic>{
		{9, 'a'}, {8, 'b'}, {7, 'c'}, {6, 'd'}, {5, 'e'},
		{4, 'f'}, {3, 'g'}, {2, 'h'}, {1, 'i'}
	});

	CHECK((vec2 | sort(asc_char)) == std::vector<ic>{
		{9, 'a'}, {8, 'b'}, {7, 'c'}, {6, 'd'}, {5, 'e'},
		{4, 'f'}, {3, 'g'}, {2, 'h'}, {1, 'i'}
	});
	CHECK((vec2 | sort(desc_char)) == std::vector<ic>{
		{1, 'i'}, {2, 'h'}, {3, 'g'}, {4, 'f'}, {5, 'e'},
		{6, 'd'}, {7, 'c'}, {8, 'b'}, {9, 'a'}
	});
}

TEST_CASE("tb::streams: reverse"){
	auto vec = std::vector<int>{1, 2, 3, 4, 5, 6, 7, 8, 9};
	CHECK((vec | reverse()) == std::vector{9, 8, 7, 6, 5, 4, 3, 2, 1});

	auto vec2 = std::vector<int>{1, 3, 2, 5, 4, 7, 6, 9, 8};
	CHECK((vec2 | reverse()) == std::vector{8, 9, 6, 7, 4, 5, 2, 3, 1});
}

TEST_CASE("tb::streams: to_string"){
	auto vec = "123cba" | to_vec();
	CHECK(vec == std::vector{'1', '2', '3', 'c', 'b', 'a'});
	CHECK((vec | to_string()) == "123cba");
	CHECK((vec | reverse() | to_string()) == "abc321");
}

TEST_CASE("tb::streams: group_by"){
	auto vec = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	auto groups = vec | group_by([](auto i){ return i % 2; });
	REQUIRE(groups.size() == 2);
	REQUIRE(groups.contains(0));
	CHECK(groups[0] == std::vector{2, 4, 6, 8, 10});
	REQUIRE(groups.contains(1));
	CHECK(groups[1] == std::vector{1, 3, 5, 7, 9});

	using strs = std::vector<std::string>;
	auto vec2 = strs{"a", "ab", "abc", "b", "bc", "bcd", "1", "12", "123"};
	auto groups2 = vec2 | group_by([](auto i){ return i.size(); });
	REQUIRE(groups2.size() == 3);
	REQUIRE(groups2.contains(1));
	CHECK(groups2[1] == strs{"a", "b", "1"});
	REQUIRE(groups2.contains(2));
	CHECK(groups2[2] == strs{"ab", "bc", "12"});
	REQUIRE(groups2.contains(3));
	CHECK(groups2[3] == strs{"abc", "bcd", "123"});
}

TEST_CASE("tb::streams: collect"){
	auto vec = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

	std::vector<int> collector;
	auto group_pairs = [&collector](auto i){
		std::optional<std::vector<int>> ret;
		collector.push_back(i);
		if(collector.size() == 2){ ret = collector; collector.clear(); }
		return ret;
	};

	auto group_triplets = [&collector](auto i){
		std::optional<std::vector<int>> ret;
		collector.push_back(i);
		if(collector.size() == 3){ ret = collector; collector.clear(); }
		return ret;
	};

	using int_vecs = std::vector<std::vector<int>>;
	CHECK((vec | collect(group_pairs) | to_vec()) == int_vecs{
		{1, 2}, {3, 4}, {5, 6}, {7, 8}, {9, 10}
	});
	CHECK(collector == std::vector<int>{});

	CHECK((vec | collect(group_triplets) | to_vec()) == int_vecs{
		{1, 2, 3}, {4, 5, 6}, {7, 8, 9}
	});
	CHECK(collector == std::vector<int>{10});
}
