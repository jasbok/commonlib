//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include <doctest/doctest.h>

#include <cstdlib>
#include <sstream>
#include <string>
#include <vector>

std::string
test_file_path(const std::string& basepath);

namespace doctest
{

template <typename T>
struct StringMaker<std::vector<T>>
{
	static String
	convert(const std::vector<T>& in) {
		std::ostringstream oss;

		if(in.empty()){
			oss << "[]";
		}
		else{
			oss << "[";

			size_t i = 0;
			for(; i < (in.size() - 1); i++){
				oss << StringMaker<T>::convert(in.at(i)) << ",";
			}
			oss << StringMaker<T>::convert(in.at(i)) << "]";
		}

		return oss.str().c_str();
	}
};

template<>
struct StringMaker<const char*>
{
	static String
	convert(const char* in) {
		std::ostringstream oss;
		oss << "\"" << in << "\"";
		return oss.str().c_str();
	}
};

template<>
struct StringMaker<std::string>
{
	static String
	convert(const std::string& in) {
		std::ostringstream oss;
		oss << "\"" << in << "\"";
		return oss.str().c_str();
	}
};

template<>
struct StringMaker<std::string_view>
{
	static String
	convert(const std::string_view& in) {
		std::ostringstream oss;
		oss << "\"" << in << "\"";
		return oss.str().c_str();
	}
};

template<typename FIRST, typename SECOND>
struct StringMaker<std::pair<FIRST, SECOND>>
{
	static String
	convert(const std::pair<FIRST, SECOND>& in) {
		std::ostringstream oss;
		oss << "{" << in.first << "," << in.second << "}";
		return oss.str().c_str();
	}
};

} // namespace doctest
