/*
 * Copyright (C) 2022 Stefan Alberts
 * This file is part of Toolbox.
 *
 * Toolbox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toolbox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
 */

#include "test/tests.h"

#include "toolbox/common/range.h"

#include <cstdlib>

TEST_CASE("tb::deduplicate: Deduplicate items in a range, keeping the ordering.") {
  std::vector<int> dups = {1, 1, 2, 1, 2, 3, 2, 1, 4, 5, 0, 9, 8, 7, 7, 6, 1};
  std::vector<int> uniques = {1, 2, 3, 4, 5, 0, 9, 8, 7, 6};
  CHECK(tb::deduplicate(dups) == uniques);
}

TEST_CASE("tb::collect: Collect items from a container into a diffent container.") {
  using if_pair = std::pair<int, float>;
  std::vector<if_pair> pairs = {
    {1, 1.1f},
    {2, 2.2f},
    {3, 3.3f},
  };

  auto get_ints = [](const if_pair& p){ return p.first; };
  auto get_floats = [](const if_pair& p){ return p.second; };

  std::vector<int> ints;
  tb::transform(pairs, ints, get_ints);

  std::vector<float> floats;
  tb::transform(pairs, floats, get_floats);

  CHECK(ints == std::vector<int>{1, 2, 3});
  CHECK(floats == std::vector<float>{1.1f, 2.2f, 3.3f});
}
