/*
 * Copyright (C) 2022 Stefan Alberts
 * This file is part of Toolbox.
 *
 * Toolbox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toolbox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
 */
#include "test/tests.h"

#include "toolbox/common/event.h"

TEST_CASE("tb::event: Test event handler.") {
  tb::event_handler<std::string> handler;
  auto pass = std::function<void()>();
  using entry = tb::mapping<std::string, decltype(pass)>;

  std::vector<entry> unsorted_mappings = {
    {"second", pass},
    {"first", pass},
    {"fifth", pass},
    {"forth", pass}
  };
  handler.insert(unsorted_mappings);

  std::vector<std::string> unsorted;
  tb::transform(handler.handles(), unsorted, [](const auto& i){ return i.key; });
  CHECK(unsorted == std::vector<std::string>{"second", "first", "fifth", "forth"});

  std::vector<std::string> ordering = {"first", "second", "third", "forth", "fifth"};
  handler.ordering(ordering);

  std::vector<std::string> sorted;
  tb::transform(handler.handles(), sorted, [](const auto& i){ return i.key; });
  CHECK(sorted == std::vector<std::string>{"first", "second", "forth", "fifth"});

  handler.insert({"third", pass});

  sorted.clear();
  tb::transform(handler.handles(), sorted, [](const auto& i){ return i.key; });
  CHECK(sorted == std::vector<std::string>{"first", "second", "third", "forth", "fifth"});

  handler.remove("second");

  sorted.clear();
  tb::transform(handler.handles(), sorted, [](const auto& i){ return i.key; });
  CHECK(sorted == std::vector<std::string>{"first", "third", "forth", "fifth"});

  handler.remove("first");

  sorted.clear();
  tb::transform(handler.handles(), sorted, [](const auto& i){ return i.key; });
  CHECK(sorted == std::vector<std::string>{"third", "forth", "fifth"});
}
