//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#include "test/tests.h"

#include "toolbox/common/str.h"

using namespace tb;
using namespace std;

TEST_CASE(
	"tb::str: Replace one defined instances of a substring in a string.")
{
	string test1 = "aa bb cc bb aa";

	CHECK(replace_one(test1, "", "") == test1);
	CHECK(replace_one(test1, "", "x") == test1);
	CHECK(replace_one(test1, "", "xx") == test1);
	CHECK(replace_one(test1, "", "xxx") == test1);

	CHECK(replace_one(test1, "b", "") == "aa b cc bb aa");
	CHECK(replace_one(test1, "b", "x") == "aa xb cc bb aa");
	CHECK(replace_one(test1,
	                  "b",
	                  "xx") == "aa xxb cc bb aa");
	CHECK(replace_one(test1,
	                  "bb",
	                  "xx") == "aa xx cc bb aa");
	CHECK(replace_one(test1, "a", "") == "a bb cc bb aa");
	CHECK(replace_one(test1, "a", "x") == "xa bb cc bb aa");
	CHECK(replace_one(test1,
	                  "a",
	                  "xx") == "xxa bb cc bb aa");
	CHECK(replace_one(test1,
	                  "aa",
	                  "xx") == "xx bb cc bb aa");
	CHECK(replace_one(test1, "c", "") == "aa bb c bb aa");
	CHECK(replace_one(test1, "c", "x") == "aa bb xc bb aa");
	CHECK(replace_one(test1,
	                  "c",
	                  "xx") == "aa bb xxc bb aa");
	CHECK(replace_one(test1,
	                  "cc",
	                  "xx") == "aa bb xx bb aa");
	CHECK(replace_one(test1, "d", "x") == "aa bb cc bb aa");
	CHECK(replace_one(test1, "d", "xx") == "aa bb cc bb aa");
	CHECK(replace_one(test1,
	                  "dd",
	                  "xx") == "aa bb cc bb aa");

	CHECK(replace_one(test1, "aa bb cc bb aa", "") == "");
	CHECK(replace_one(test1, "aa bb cc bb aa", "x") == "x");
	CHECK(replace_one(test1,
	                  "aa bb cc bb aa",
	                  "xx") == "xx");
	CHECK(replace_one(test1,
	                  "aa bb cc bb aa",
	                  "xx xx xx xx xx") == "xx xx xx xx xx");
	CHECK(replace_one(test1,
	                  "aa bb cc bb aa",
	                  "yy xx xx xx xx xx yy") ==
	      "yy xx xx xx xx xx yy");

	string test2   =
		R"([[config(\"This is a test case 2.\")]])";
	string result2 =
		R"([[config("This is a test case 2.\")]])";
	CHECK(replace_one(test2, "\\\"", "\"") == result2);

	string test3   =
		R"([[config("This is a test case 2.")]])";
	string result3 =
		R"([[config(\"This is a test case 2.")]])";
	CHECK(replace_one(test3, "\"", "\\\"") == result3);
}

TEST_CASE(
	"tb::str: Replace all defined instances of a substring in a string.")
{
	string test1 = "aa bb cc bb aa";

	CHECK(replace_all(test1, "", "") == test1);
	CHECK(replace_all(test1, "", "x") == test1);
	CHECK(replace_all(test1, "", "xx") == test1);
	CHECK(replace_all(test1, "", "xxx") == test1);

	CHECK(replace_all(test1, "b", "") == "aa  cc  aa");
	CHECK(replace_all(test1, "b", "x") == "aa xx cc xx aa");
	CHECK(replace_all(test1,
	                  "b",
	                  "xx") == "aa xxxx cc xxxx aa");
	CHECK(replace_all(test1,
	                  "bb",
	                  "xx") == "aa xx cc xx aa");
	CHECK(replace_all(test1, "a", "") == " bb cc bb ");
	CHECK(replace_all(test1, "a", "x") == "xx bb cc bb xx");
	CHECK(replace_all(test1,
	                  "a",
	                  "xx") == "xxxx bb cc bb xxxx");
	CHECK(replace_all(test1,
	                  "aa",
	                  "xx") == "xx bb cc bb xx");
	CHECK(replace_all(test1, "c", "") == "aa bb  bb aa");
	CHECK(replace_all(test1, "c", "x") == "aa bb xx bb aa");
	CHECK(replace_all(test1,
	                  "c",
	                  "xx") == "aa bb xxxx bb aa");
	CHECK(replace_all(test1,
	                  "cc",
	                  "xx") == "aa bb xx bb aa");
	CHECK(replace_all(test1, "d", "x") == "aa bb cc bb aa");
	CHECK(replace_all(test1, "d", "xx") == "aa bb cc bb aa");
	CHECK(replace_all(test1,
	                  "dd",
	                  "xx") == "aa bb cc bb aa");

	CHECK(replace_all(test1, "aa bb cc bb aa", "") == "");
	CHECK(replace_all(test1, "aa bb cc bb aa", "x") == "x");
	CHECK(replace_all(test1,
	                  "aa bb cc bb aa",
	                  "xx") == "xx");
	CHECK(replace_all(test1,
	                  "aa bb cc bb aa",
	                  "xx xx xx xx xx") == "xx xx xx xx xx");
	CHECK(replace_all(test1,
	                  "aa bb cc bb aa",
	                  "yy xx xx xx xx xx yy") ==
	      "yy xx xx xx xx xx yy");

	string test2   = R"([[config(\"This is a test case 2.\")]])";
	string result2 = R"([[config("This is a test case 2.")]])";
	CHECK(replace_all(test2, "\\\"", "\"") == result2);

	string test3   = R"([[config("This is a test case 2.")]])";
	string result3 = R"([[config(\"This is a test case 2.\")]])";
	CHECK(replace_all(test3, "\"", "\\\"") == result3);
}

TEST_CASE(
	"tb::str: Replace all defined regex instances of a substring in a string.")
{
	string test1 = "aa bb cc bb aa";

	// CHECK(replace_all(test1, regex(""), "") == test1);
	// CHECK(replace_all(test1, regex(""), "x") == test1);
	// CHECK(replace_all(test1, regex(""), "xx") == test1);
	// CHECK(replace_all(test1, regex(""), "xxx") == test1);

	regex b("b");
	regex bb("bb");
	regex bp("b+");

	CHECK(replace_all(test1, b, "") == "aa  cc  aa");
	CHECK(replace_all(test1, b, "x") == "aa xx cc xx aa");
	CHECK(replace_all(test1,
	                  b,
	                  "xx") == "aa xxxx cc xxxx aa");
	CHECK(replace_all(test1, bb, "xx") == "aa xx cc xx aa");
	CHECK(replace_all(test1, bb, "x") == "aa x cc x aa");
	CHECK(replace_all(test1, bp, "x") == "aa x cc x aa");
	CHECK(replace_all(test1, bp, "xx") == "aa xx cc xx aa");

	regex a("a");
	regex aa("aa");
	regex ap("a+");
	CHECK(replace_all(test1, a, "") == " bb cc bb ");
	CHECK(replace_all(test1, a, "x") == "xx bb cc bb xx");
	CHECK(replace_all(test1,
	                  a,
	                  "xx") == "xxxx bb cc bb xxxx");
	CHECK(replace_all(test1, aa, "x") == "x bb cc bb x");
	CHECK(replace_all(test1, aa, "xx") == "xx bb cc bb xx");
	CHECK(replace_all(test1, ap, "x") == "x bb cc bb x");
	CHECK(replace_all(test1, ap, "xx") == "xx bb cc bb xx");

	regex c("c");
	regex cc("cc");
	regex cp("c+");
	CHECK(replace_all(test1, c, "") == "aa bb  bb aa");
	CHECK(replace_all(test1, c, "x") == "aa bb xx bb aa");
	CHECK(replace_all(test1, c, "xx") == "aa bb xxxx bb aa");
	CHECK(replace_all(test1, cc, "xx") == "aa bb xx bb aa");
	CHECK(replace_all(test1, cc, "x") == "aa bb x bb aa");
	CHECK(replace_all(test1, cp, "x") == "aa bb x bb aa");
	CHECK(replace_all(test1, cp, "xx") == "aa bb xx bb aa");

	regex d("d");
	regex dd("dd");
	regex dp("d+");
	CHECK(replace_all(test1, d, "x") == "aa bb cc bb aa");
	CHECK(replace_all(test1, dd, "xx") == "aa bb cc bb aa");

	CHECK(replace_all(test1,
	                  regex("aa bb cc bb aa"),
	                  "") == "");
	CHECK(replace_all(test1,
	                  regex("aa bb cc bb aa"),
	                  "x") == "x");
	CHECK(replace_all(test1,
	                  regex("aa bb cc bb aa"),
	                  "xx") == "xx");
	CHECK(replace_all(test1,
	                  regex("aa bb cc bb aa"),
	                  "xx xx xx xx xx") == "xx xx xx xx xx");
	CHECK(replace_all(test1,
	                  regex(
				  "aa bb cc bb aa"),
	                  "yy xx xx xx xx xx yy") ==
	      "yy xx xx xx xx xx yy");

	string test2   =
		R"([[config(\"This is a test case 2.\")]])";
	string result2 =
		R"([[config("This is a test case 2.")]])";
	CHECK(replace_all(test2,
	                  regex(
				  R"(\\\")"),
	                  "\"") == result2);

	string test3   =
		R"([[config("This is a test case 2.")]])";
	string result3 =
		R"([[config(\"This is a test case 2.\")]])";
	CHECK(replace_all(test3,
	                  regex(
				  R"(\")"),
	                  "\\\"") == result3);

	auto test4 =
		R"(
    // A comment
    [[config]]
    [[config()]]
    [[config(123)]]
    [[config("This is a test case.")]]
    int test(){ return 1; }
  )";

	auto result4 =
		R"|(
    // A comment
    [[clang::annotate(R"([[config]])")]]
    [[clang::annotate(R"([[config()]])")]]
    [[clang::annotate(R"([[config(123)]])")]]
    [[clang::annotate(R"([[config("This is a test case.")]])")]]
    int test(){ return 1; }
  )|";

	regex regex4("\\[\\[(.+?)\\]\\]");
	string fmt4(R"|([[clang::annotate(R"($0)")]])|");

	CHECK(replace_all(test4, regex4, fmt4) == result4);
}

TEST_CASE("tb::str: Split strings using a string delimiter.") {
	CHECK(split("a,b,c",
	            ",") ==
	      vector<string>{ "a", "b", "c" });
	CHECK(split("a,,c",
	            ",") ==
	      vector<string>{ "a", "", "c" });
	CHECK(split("a",
	            ",") == vector<string>{ "a" });
	CHECK(split("a,",
	            ",") == vector<string>{ "a", "" });
	CHECK(split("", ",") == vector<string>{ "" });

	CHECK(split("a,b,c",
	            ".") == vector<string>{ "a,b,c" });
	CHECK(split("a,,c",
	            ".") == vector<string>{ "a,,c" });
	CHECK(split("a",
	            ".") == vector<string>{ "a" });
	CHECK(split("a,",
	            ".") == vector<string>{ "a," });
	CHECK(split("", ".") == vector<string>{ "" });

	CHECK(split("a,b,c",
	            ",,") ==
	      vector<string>{ "a,b,c" });
	CHECK(split("a,,c",
	            ",,") ==
	      vector<string>{ "a", "c" });
	CHECK(split("a",
	            ",,") == vector<string>{ "a" });
	CHECK(split("a,",
	            ",,") == vector<string>{ "a," });
	CHECK(split("", ",,") == vector<string>{ "" });

	CHECK(split("a,b,c",
	            "") == vector<string>{ "a,b,c" });
	CHECK(split("a,,c",
	            "") == vector<string>{ "a,,c" });
	CHECK(split("a", "") == vector<string>{ "a" });
	CHECK(split("a,",
	            "") == vector<string>{ "a," });
	CHECK(split("", "") == vector<string>{ "" });
}

TEST_CASE("tb::str: Find regex groups.") {
	regex attr_list_regex(
		R"|(\[{2}\s*(?:using\s+(\w+)\s*:)?\s*(.*)\s*\]{2})|");
	regex attr_regex(
		R"|((?:(\w*)\s*::\s*)?(\w+)(?:\s*\(\s*(.*)\s*\))?)|");

	auto str_a =
		R"|(
        [[attr1]]
        [[attr2(args2)]]
        [[namespace3::attr3]]
        [[namespace4::attr4(args4)]]
        [[using namespace5: attr5]]
        [[using namespace6: attr6(args6)]]
        [[using namespace7: attr7, attr8(args8)]]
    )|";

	auto str_b =
		R"|(
        attr1
        attr2(args2)
        namespace3::attr3
        namespace4::attr4(args4)
    )|";

	vector<vector<string>> expected_a = {
		{ "[[attr1]]",                                 "",
		  "attr1"                    },
		{ "[[attr2(args2)]]",                          "",
		  "attr2(args2)"             },
		{ "[[namespace3::attr3]]",                     "",
		  "namespace3::attr3"        },
		{ "[[namespace4::attr4(args4)]]",              "",
		  "namespace4::attr4(args4)" },
		{ "[[using namespace5: attr5]]",               "namespace5",
		  "attr5"                    },
		{ "[[using namespace6: attr6(args6)]]",        "namespace6",
		  "attr6(args6)"             },
		{ "[[using namespace7: attr7, attr8(args8)]]", "namespace7",
		  "attr7, attr8(args8)"      },
	};

	vector<vector<string>> expected_b = {
		{ "attr1",                    "",           "attr1", ""      },
		{ "attr2(args2)",             "",           "attr2", "args2" },
		{ "namespace3::attr3",        "namespace3", "attr3", ""      },
		{ "namespace4::attr4(args4)", "namespace4", "attr4", "args4" },
	};

	CHECK(expected_a == find_all_groups(str_a,
	                                    attr_list_regex) );
	CHECK(expected_b == find_all_groups(str_b, attr_regex) );
}

TEST_CASE("tb::str: Join a vector of strings using a separator.") {
	vector<string> vec_a = {};
	vector<string> vec_b = { "str_a" };
	vector<string> vec_c = { "str_a", "str_b" };
	vector<string> vec_d = { "str_a", "str_b", "str_c" };

	CHECK(join(vec_a, "::") == "");
	CHECK(join(vec_b, "::") == "str_a");
	CHECK(join(vec_c, "::") == "str_a::str_b");
	CHECK(join(vec_d, "::") == "str_a::str_b::str_c");
}

TEST_CASE("tb::str: Trim whitespace from start and end of string.") {
	auto empty           = "";
	auto no_whitespace   = "no_whitespace";
	auto space_start     = " space_start";
	auto space_end       = "space_end ";
	auto space_start_end = " space_start_end ";
	auto space_middle    = "space middle";
	auto start_end       = " start end ";
	auto tabs            = "\t\ttabs\t\t";
	auto newlines        = "\n\nnewlines\n\n";
	auto mixed           = " \t\n\n\t mixed \t\n\n\t ";

	CHECK(trim(empty) == "");
	CHECK(trim(no_whitespace) == "no_whitespace");
	CHECK(trim(space_start) == "space_start");
	CHECK(trim(space_end) == "space_end");
	CHECK(trim(space_start_end) == "space_start_end");
	CHECK(trim(space_middle) == "space middle");
	CHECK(trim(start_end) == "start end");
	CHECK(trim(tabs) == "tabs");
	CHECK(trim(newlines) == "newlines");
	CHECK(trim(mixed) == "mixed");

	CHECK(trim("") == "");
	CHECK(trim("no_whitespace") == "no_whitespace");
	CHECK(trim(" space_start") == "space_start");
	CHECK(trim("space_end ") == "space_end");
	CHECK(trim(" space_start_end ") == "space_start_end");
	CHECK(trim("space middle") == "space middle");
	CHECK(trim(" start end ") == "start end");
	CHECK(trim("\t\ttabs\t\t") == "tabs");
	CHECK(trim("\n\nnewlines\n\n") == "newlines");
	CHECK(trim(" \t\n\n\t mixed \t\n\n\t ") == "mixed");
}

TEST_CASE("tb::str: Check if a string starts with a character or sequence")
{
	CHECK(starts_with("", "") == true);
	CHECK(starts_with("", 'a') == false);
	CHECK(starts_with("", "a") == false);
	CHECK(starts_with("", "ab") == false);
	CHECK(starts_with("", "abc") == false);

	CHECK(starts_with("a", "") == true);
	CHECK(starts_with("a", 'a') == true);
	CHECK(starts_with("a", "a") == true);
	CHECK(starts_with("a", "ab") == false);
	CHECK(starts_with("a", "abc") == false);

	CHECK(starts_with("b", "") == true);
	CHECK(starts_with("b", 'a') == false);
	CHECK(starts_with("b", "a") == false);
	CHECK(starts_with("b", "ab") == false);
	CHECK(starts_with("b", "abc") == false);

	CHECK(starts_with("ab", "") == true);
	CHECK(starts_with("ab", 'a') == true);
	CHECK(starts_with("ab", "a") == true);
	CHECK(starts_with("ab", "ab") == true);
	CHECK(starts_with("ab", "abc") == false);

	CHECK(starts_with("abc", "") == true);
	CHECK(starts_with("abc", 'a') == true);
	CHECK(starts_with("abc", "a") == true);
	CHECK(starts_with("abc", "ab") == true);
	CHECK(starts_with("abc", "abc") == true);

	CHECK(starts_with("abcd", "") == true);
	CHECK(starts_with("abcd", 'a') == true);
	CHECK(starts_with("abcd", "a") == true);
	CHECK(starts_with("abcd", "ab") == true);
	CHECK(starts_with("abcd", "abc") == true);

	CHECK(starts_with(" abcd", "") == true);
	CHECK(starts_with(" abcd", 'a') == false);
	CHECK(starts_with(" abcd", "a") == false);
	CHECK(starts_with(" abcd", "ab") == false);
	CHECK(starts_with(" abcd", "abc") == false);
	CHECK(starts_with(" abcd", " a") == true);
	CHECK(starts_with(" abcd", " ab") == true);
	CHECK(starts_with(" abcd", " abc") == true);
	CHECK(starts_with(" abcd", " abcd") == true);

	CHECK(starts_with("abcd ", "") == true);
	CHECK(starts_with("abcd ", 'a') == true);
	CHECK(starts_with("abcd ", "a") == true);
	CHECK(starts_with("abcd ", "ab") == true);
	CHECK(starts_with("abcd ", "abc") == true);
	CHECK(starts_with("abcd ", "abcd") == true);
}

TEST_CASE("tb::str: Check if a string ends with a character or sequence.")
{
	CHECK(ends_with("", "") == true);
	CHECK(ends_with("", 'a') == false);
	CHECK(ends_with("", "a") == false);
	CHECK(ends_with("", "ab") == false);
	CHECK(ends_with("", "abc") == false);

	CHECK(ends_with("a", "") == true);
	CHECK(ends_with("a", 'a') == true);
	CHECK(ends_with("a", "ab") == false);
	CHECK(ends_with("a", "abc") == false);

	CHECK(ends_with("b", "") == true);
	CHECK(ends_with("b", 'a') == false);
	CHECK(ends_with("b", "a") == false);
	CHECK(ends_with("b", "ab") == false);
	CHECK(ends_with("b", "abc") == false);
	CHECK(ends_with("b", "b") == true);

	CHECK(ends_with("ab", "") == true);
	CHECK(ends_with("ab", 'a') == false);
	CHECK(ends_with("ab", "a") == false);
	CHECK(ends_with("ab", "ab") == true);
	CHECK(ends_with("ab", "abc") == false);
	CHECK(ends_with("ab", "b") == true);
	CHECK(ends_with("ab", "ab") == true);

	CHECK(ends_with("abc", "") == true);
	CHECK(ends_with("abc", 'a') == false);
	CHECK(ends_with("abc", "a") == false);
	CHECK(ends_with("abc", "ab") == false);
	CHECK(ends_with("abc", "abc") == true);
	CHECK(ends_with("abc", "c") == true);
	CHECK(ends_with("abc", "bc") == true);

	CHECK(ends_with("abcd", "") == true);
	CHECK(ends_with("abcd", 'a') == false);
	CHECK(ends_with("abcd", "a") == false);
	CHECK(ends_with("abcd", "ab") == false);
	CHECK(ends_with("abcd", "abc") == false);
	CHECK(ends_with("abcd", "d") == true);
	CHECK(ends_with("abcd", "cd") == true);
	CHECK(ends_with("abcd", "bcd") == true);
	CHECK(ends_with("abcd", "abcd") == true);

	CHECK(ends_with(" abcd", "") == true);
	CHECK(ends_with(" abcd", 'a') == false);
	CHECK(ends_with(" abcd", "a") == false);
	CHECK(ends_with(" abcd", "ab") == false);
	CHECK(ends_with(" abcd", "abc") == false);
	CHECK(ends_with(" abcd", "d") == true);
	CHECK(ends_with(" abcd", "cd") == true);
	CHECK(ends_with(" abcd", "bcd") == true);
	CHECK(ends_with(" abcd", "abcd") == true);

	CHECK(ends_with("abcd ", "") == true);
	CHECK(ends_with("abcd ", 'a') == false);
	CHECK(ends_with("abcd ", "a") == false);
	CHECK(ends_with("abcd ", "ab") == false);
	CHECK(ends_with("abcd ", "abc") == false);
	CHECK(ends_with("abcd ", "d") == false);
	CHECK(ends_with("abcd ", "cd") == false);
	CHECK(ends_with("abcd ", "bcd") == false);
	CHECK(ends_with("abcd ", "abcd") == false);
	CHECK(ends_with("abcd ", "d ") == true);
	CHECK(ends_with("abcd ", "cd ") == true);
	CHECK(ends_with("abcd ", "bcd ") == true);
	CHECK(ends_with("abcd ", "abcd ") == true);
}

TEST_CASE("tb::str: Split string with braces.") {
	string normal_args = R"(arg1, arg2, arg3, arg4)";

	CHECK(split(normal_args,
	            ",",
	            { { "\"", "\"" } }) == vector<string>{
		"arg1",
		" arg2",
		" arg3",
		" arg4"
	});

	string normal_args_with_empties = R"(,arg1, arg2,, arg3, arg4,,)";
	CHECK(split(normal_args_with_empties,
	            ",",
	            { { "\"", "\"" } }) == vector<string>{
		"",
		"arg1",
		" arg2",
		"",
		" arg3",
		" arg4",
		"",
		""
	});

	string quoted_args = R"(arg1, "arg2", "arg3, arg4", ",")";
	CHECK(split(quoted_args,
	            ",",
	            { { "\"", "\"" } }) == vector<string>{
		"arg1",
		" \"arg2\"",
		" \"arg3, arg4\"",
		" \",\""
	});

	string quoted_args_with_empties =
		R"(,arg1, "", "arg2", "arg3,, arg4", ",",,)";
	CHECK(split(quoted_args_with_empties,
	            ",",
	            { { "\"", "\"" } }) == vector<string>{
		"",
		"arg1",
		" \"\"",
		" \"arg2\"",
		" \"arg3,, arg4\"",
		" \",\"",
		"",
		""
	});

	string braces_args =
		R"(arg1, {arg2}, {arg3, arg4}, {arg5, {arg6, arg7}, arg8})";
	CHECK(split(braces_args,
	            ",",
	            { { "{", "}" } }) == vector<string>{
		"arg1",
		" {arg2}",
		" {arg3, arg4}",
		" {arg5, {arg6, arg7}, arg8}",
	});

	string braces_args_with_empties =
		R"(,arg1, {}, {{}}, {arg2}, {arg3,, arg4}, {arg5, {arg6, arg7}, arg8},,)";
	CHECK(split(braces_args_with_empties,
	            ",",
	            { { "{", "}" } }) == vector<string>{
		"",
		"arg1",
		" {}",
		" {{}}",
		" {arg2}",
		" {arg3,, arg4}",
		" {arg5, {arg6, arg7}, arg8}",
		"",
		"",
	});
}

TEST_CASE("tb::str: Read key-value pairs."){
	auto empty_str = tb::read_kv("", '=');
	CHECK(empty_str.first.empty());
	CHECK(empty_str.second.empty());
	CHECK(empty_str.second == "");

	auto empty_kv = tb::read_kv("=", '=');
	CHECK(empty_kv.first.empty()); CHECK(empty_kv.second.empty());

	auto empty_key = tb::read_kv("=value", '=');
	CHECK(empty_key.first.empty()); CHECK(empty_key.second == "value");

	auto empty_value = tb::read_kv("key=", '=');
	CHECK(empty_value.first == "key"); CHECK(empty_value.second.empty());

	auto key_value = tb::read_kv("key=value", '=');
	CHECK(key_value.first == "key"); CHECK(key_value.second == "value");

	auto key_value_wrong_sep = tb::read_kv("key=value", ';');
	CHECK(key_value_wrong_sep.first == "key=value");
	CHECK(key_value_wrong_sep.second.empty());
	CHECK(key_value_wrong_sep.second == "");

	auto key_value_ws = tb::read_kv("key = value", '=');
	CHECK(key_value_ws.first == "key "); CHECK(key_value_ws.second == " value");

	auto key_value_ws_trim = tb::read_kv_trim("key = value", '=');
	CHECK(key_value_ws_trim.first == "key");
	CHECK(key_value_ws_trim.second == "value");
}
