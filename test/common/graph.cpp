//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#include "toolbox/common/graph.h"

#include <algorithm>
#include <string>

int
main(){
	auto gr = tb::directional_graph_from<std::string>({
		{"first", {"second", "fourth"}},
		{"second", {"third"}},
		{"third", {}},
		{"fourth", {"fifth"}},
		{"fifth", {}},
		{"sixth", {"fourth"}},
	});

	printf("Travel node first from 'first': ");
	travel<std::string>(gr, 0, [](const auto& n){
		printf("%s -> ", n.data.c_str());
	});
	printf("END\n");

	printf("Travel edge first from 'first': ");
	travel_edge_first<std::string>(gr, 0, [](const auto& n){
		printf("%s -> ", n.data.c_str());
	});
	printf("END\n\n");

	printf("Travel node first from 'sixth': ");
	travel<std::string>(gr, 5, [](const auto& n){
		printf("%s -> ", n.data.c_str());
	});
	printf("END\n");

	printf("Travel edge first from 'sixth': ");
	travel_edge_first<std::string>(gr, 5, [](const auto& n){
		printf("%s -> ", n.data.c_str());
	});
	printf("END\n\n");

	tb::node_edges_map<std::string> map = {
		{"first", {"second", "fourth"}},
		{"second", {"third"}},
		{"third", {"third", "fifth"}},
		{"fourth", {"fifth"}},
		{"fifth", {"first", "fourth"}},
		{"sixth", {"fourth", "seventh"}},
	};
	gr = tb::directional_graph_from<std::string>(map);

	for(size_t n = 0; n < map.size(); n++) {
		printf("Loops starting from '%s':\n", map[n].first.c_str());
		find_loops(gr, n, [&gr](std::vector<size_t>&& path, std::vector<size_t>&& loop){
			printf("Path: ");
			for(size_t i : path) {
				printf("%s -> ", gr[i].data.c_str());
			}

			printf("Loop: ");
			for(size_t i : loop) {
				printf("%s -> ", gr[i].data.c_str());
			}
			printf("%s\n", gr[loop[0]].data.c_str());
		});
		printf("\n");
	}

} // main
