//
// Copyright (C) 2021 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#include "test/tests.h"

#include "toolbox/json.h"

using namespace tb;
using namespace std;

using json = nlohmann::json;

struct object {
	int integer;
	float floating;
	std::string string;
	vector<float> array;
};

struct object_nested {
	int integer;
	float floating;
	std::string string;
	vector<float> array;
	struct object object;
};

struct object json_object = { 2, 2.2f, "a string", {4.4f, 5.5f, 5.6f}};

struct object_nested json_object_nested =
{ 1, 1.1f, "a string", {1.1f, 2.2f, 3.3f}, json_object};

static string json_str =
	R"({
  "integer": 1,
  "floating": 1.1,
  "string": "a string",
  "array": [1.1, 2.2, 3.3],
  "object": {
    "integer": 2,
    "floating": 2.2,
    "string": "another string",
    "array": [4.4, 5.5, 6.6]
  }
}
)";

void
to_json(json& j, const struct object& o) {
	j = {
		{"integer", o.integer },
		{"floating", o.floating },
		{"string", o.string },
		{"array", o.array },
	};
}

void
to_json(json& j, const struct object_nested& o) {
	j = {
		{"integer", o.integer },
		{"floating", o.floating },
		{"string", o.string },
		{"array", o.array },
		{"object", o.object },
	};
}

void
from_json(const json& j, struct object& o) {
	o.integer = j.at("integer").get<int>();
	o.floating = j.at("floating").get<float>();
	o.string = j.at("string").get<string>();
	o.array = j.at("array").get<vector<float>>();
}

void
from_json(const json& j, struct object_nested& o) {
	o.integer = j.at("integer").get<int>();
	o.floating = j.at("floating").get<float>();
	o.string = j.at("string").get<string>();
	o.array = j.at("array").get<vector<float>>();
	o.object = j.at("object").get<struct object>();
}

TEST_CASE ( "tb::nlohmann::json: Perform various JSON conversions." )
{
	using namespace tb;
	using namespace std;

	json j = json::parse(json_str);
	REQUIRE(j.is_object() == true);

	CHECK_THROWS_AS(get_number<int>(j), const expected_number&);
	CHECK_THROWS_AS(get_number<float>(j), const expected_number&);
	CHECK_THROWS_AS(get_string<string>(j), const expected_string&);
	CHECK_THROWS_AS(get_array<vector<int>>(j), const expected_array&);
	CHECK_NOTHROW(get_object<struct object>(j));
	CHECK_NOTHROW(get_object<struct object_nested>(j));

	CHECK_NOTHROW(get_number<int>(j,"integer"));
	CHECK_NOTHROW(get_number<int>(j,"floating"));
	CHECK_THROWS_AS(get_number<int>(j,"string"),
	                const expected_number_field&);
	CHECK_THROWS_AS(get_number<int>(j,"array"),
	                const expected_number_field&);
	CHECK_THROWS_AS(get_number<int>(j,"object"),
	                const expected_number_field&);

	CHECK_NOTHROW(get_number<float>(j,"integer"));
	CHECK_NOTHROW(get_number<float>(j,"floating"));
	CHECK_THROWS_AS(get_number<float>(j,"string"),
	                const expected_number_field&);
	CHECK_THROWS_AS(get_number<float>(j,"array"),
	                const expected_number_field&);
	CHECK_THROWS_AS(get_number<float>(j,"object"),
	                const expected_number_field&);

	CHECK_THROWS_AS(get_string<string>(j,"integer"),
	                const expected_string_field&);
	CHECK_THROWS_AS(get_string<string>(j,"floating"),
	                const expected_string_field&);
	CHECK_NOTHROW(get_string<string>(j,"string"));
	CHECK_THROWS_AS(get_string<string>(j,"array"),
	                const expected_string_field&);
	CHECK_THROWS_AS(get_string<string>(j,"object"),
	                const expected_string_field&);

	CHECK_THROWS_AS(get_array<vector<int>>(j,"integer"),
	                const expected_array_field&);
	CHECK_THROWS_AS(get_array<vector<int>>(j,"floating"),
	                const expected_array_field&);
	CHECK_THROWS_AS(get_array<vector<int>>(j,"string"),
	                const expected_array_field&);
	CHECK_NOTHROW(get_array<vector<int>>(j,"array"));
	CHECK_THROWS_AS(get_array<vector<int>>(j,"object"),
	                const expected_array_field&);


	CHECK_THROWS_AS(get_object<object>(j,"integer"),
	                const expected_object_field&);
	CHECK_THROWS_AS(get_object<object>(j,"floating"),
	                const expected_object_field&);
	CHECK_THROWS_AS(get_object<object>(j,"string"),
	                const expected_object_field&);
	CHECK_THROWS_AS(get_object<object>(j,"array"),
	                const expected_object_field&);
	CHECK_NOTHROW(get_object<object>(j,"object"));

	CHECK(get_number<int>(j, "integer") == 1);
	CHECK(get_number<int>(j, "floating") == 1);
	CHECK(get_number<float>(j, "integer") == 1.0f);
	CHECK(get_number<float>(j, "floating") == 1.1f);
	CHECK(get_string<string>(j, "string") == "a string");
	CHECK(get_array<vector<int>>(j, "array")
	      == vector<int>{1, 2, 3});
	CHECK(get_array<vector<float>>(j, "array")
	      == vector<float>{1.1f, 2.2f, 3.3f});

	auto object_field = get_object<object>(j, "object");
	CHECK(get_number<int>(object_field, "integer") == 2);
	CHECK(get_number<int>(object_field, "floating") == 2);
	CHECK(get_number<float>(object_field, "integer") == 2.0f);
	CHECK(get_number<float>(object_field, "floating") == 2.2f);
	CHECK(get_string<string>(object_field, "string") == "another string");
	CHECK(get_array<vector<int>>(object_field, "array")
	      == vector<int>{4, 5, 6});
	CHECK(get_array<vector<float>>(object_field, "array")
	      == vector<float>{4.4f, 5.5f, 6.6f});
	CHECK_THROWS_AS(get_object<object>(object_field, "object"),
	                const expected_field);
}
