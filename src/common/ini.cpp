//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#include "toolbox/common/ini.h"

#include "toolbox/common/file.h"
#include "toolbox/common/str.h"

#include <cctype>
#include <optional>
#include <stdexcept>
#include <string_view>

namespace tb {

std::string
read_key(const std::string_view& str){
	std::string key; key.reserve(str.size());

	size_t i = 0;
	for(; isspace(str[i]) && i < str.size(); i++);
	if(i == str.size()){ return ""; }; // Only whitespace.

	bool quo = (str[i] == '"'); if(quo){i++;}  // Check if str is quoted.
	bool esc = false;             // Escaped character switch.
	for(; i < str.size(); i++){
		char c = str[i];
		if(esc){ esc = false; key.push_back(c); continue; }         // Always consume escape chars.
		if(c == '\\'){ esc = true; continue; }                      // Don't consume slash.
		if(!quo && (isspace(c) || c == '#' || c == ';')){ break; }  // Comment or end of non-quoted key.
		if(c == '"' && quo){ break; }                               // End of quote.
		if(c == '"' && !quo){                                       // Double quote not first character in key.
			throw std::runtime_error("Invalid key: " + std::string(str));
		}
		key.push_back(c);
	}

	return std::string(trim(key));
}

std::string_view
strip_inline_comments(const std::string_view& line){
	bool quo = false; bool esc = false;
	auto it = std::find_if(begin(line), end(line), [&quo, &esc](char c){
		if(c == '\\'){ esc = !esc; return false; }
		if(c == '"' && !esc){ quo = !quo; return false; }
		esc = !esc;
		return !quo && (c == '#' || c == ';');
	});

	return {begin(line), it};
}

std::string_view
remove_quotes(const std::string_view& line){
	if(line.size() < 2 || line[0] != '"' || line[line.size()] != '"'){
		return line;
	}
	return {begin(line) + 1, end(line) - 1};
}

std::optional<std::pair<std::string_view, std::string_view>>
read_ini_kv(const std::string_view& line){
	// Read and trim key-values.
	auto kv = read_kv_trim(line, '=');

	// Skip invalid lines.
	if(kv.first.empty() || kv.second.empty()){ return std::nullopt; }

	return {{kv.first, kv.second}};
}

void
set_or_replace(ini::section& section, const ini::kv& kv){
	auto& kvs = section.key_values;
	auto it = std::find_if(begin(kvs), end(kvs), [&kv](const auto& i){
		return i.key == kv.key;
	});

	if(it != end(kvs)){ it->value = kv.value; return; }
	kvs.push_back(kv);
}

std::optional<std::string>
read_ini_section_name(const std::string_view& line){
	// Check if line is a valid section.
	if(!line.starts_with('[') || !line.ends_with(']')){ return std::nullopt; }

	// Handle empty sections.
	if(line == "[]"){ return ""; }

	return {std::string(trim(std::string_view{begin(line)+1, end(line)-1}))};
}

size_t
find_or_create(std::vector<ini::section>& vec,
               const std::string_view& name){
	for(size_t i = 0; i < vec.size(); i++){ if(vec[i].name == name){ return i;} }

	vec.push_back({.name = std::string(name)});
	return vec.size() - 1;
}

ini
read_ini(const std::string& path){
	std::vector<ini::section> sections{1};
	size_t curr = 0;

	read_ini(path, [&sections, &curr](auto& sec, auto& key, auto& val){
		if(sections[curr].name != sec){ curr = find_or_create(sections, sec); }

		using str = std::string;
		set_or_replace(sections[curr], {.key = str(key), .value = str(val)});
	});

	return {.sections = sections};
}

void
read_ini(const std::string& path,
         std::function<void(const std::string_view& section,
                            const std::string_view& key,
                            const std::string_view& value)> cb){
	std::string section = "";

	tb::read_lines(path, [&section, &cb](const std::string_view& line){
		auto tline = trim(line);

		// Skip comments
		if(tline.starts_with('#') || tline.starts_with(';')){ return; }

		// Read key-values and store.
		if(auto kv = read_ini_kv(tline)){
			cb(section, kv->first, read_key(kv->second)); return;
		}

		// Read section names and change current section.
		if(auto name = read_ini_section_name(tline)){ section = *name; }
	});
}

ini::section&
ini::operator[](const std::string_view& name){
	for(auto& section : sections){ if(section.name == name){ return section; }}
	throw std::runtime_error(std::string("Section not found in ini: ").append(name));
}

std::vector<ini::section>::iterator
ini::begin(){ return sections.begin(); }

std::vector<ini::section>::iterator
ini::end(){ return sections.end(); }

std::string&
ini::section::operator[](const std::string_view& key){
	for(auto& kv : key_values){ if(kv.key == key){ return kv.value; }}
	throw std::runtime_error(std::string("Key not found in section: ").append(key));
}

std::vector<ini::kv>::iterator
ini::section::begin(){ return key_values.begin(); }

std::vector<ini::kv>::iterator
ini::section::end(){ return key_values.end(); }

} // namespace tb
