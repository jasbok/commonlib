//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#include "toolbox/common/file.h"

#include <fstream>

namespace tb {
file_read_exception::
file_read_exception(const std::string& mesg) : mesg_(mesg)
{}

const char *
file_read_exception::
what() const throw() {
	return mesg_.c_str();
}

std::string
read_file(const std::string& path) {
	if(std::ifstream ifs(path); ifs.is_open()){
		std::string content;
		content.assign((std::istreambuf_iterator<char>(ifs)),
		               (std::istreambuf_iterator<char>()));
		return content;
	}
	throw file_read_exception("Could not open file: " + path);
}

std::vector<std::string>
read_lines(const std::string& path) {
	if(std::ifstream ifs(path); ifs.is_open()){
		std::vector<std::string> lines;
		for(std::string line; std::getline(ifs, line); lines.push_back(line));
		return lines;
	}
	throw file_read_exception("Could not open file: " + path);
}

void
read_lines(const std::string& path,
           std::function<void(const std::string_view&)> func){
	if(std::ifstream ifs(path); ifs.is_open()){
		for(std::string line; std::getline(ifs, line); func(line));
		return;
	}
	throw file_read_exception("Could not open file: " + path);
}

}; // namespace sqr
