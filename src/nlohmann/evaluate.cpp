//
// Copyright (C) 2021 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#include "toolbox/nlohmann/evaluate.h"

namespace tb {
using namespace nlohmann;

bool
is_object(const nlohmann::json& j) {
	return j.is_object();
}

bool
is_array(const nlohmann::json& j) {
	return j.is_array();
}

bool
is_string(const nlohmann::json& j) {
	return j.is_string();
}

bool
is_number(const nlohmann::json& j) {
	return j.is_number();
}

bool
has_field(const nlohmann::json& j, const std::string& field) {
	return is_object(j) && j.contains(field);
}

bool
is_object(const nlohmann::json& j, const std::string& field) {
	return has_field(j, field) && is_object(j.at(field));
}

bool
is_array(const nlohmann::json& j, const std::string& field) {
	return has_field(j, field) && is_array(j.at(field));
}

bool
is_string(const nlohmann::json& j, const std::string& field) {
	return has_field(j, field) && is_string(j.at(field));
}

bool
is_number(const nlohmann::json& j, const std::string& field) {
	return has_field(j, field) && is_number(j.at(field));
}
} // namespace tb
