//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#include "toolbox/nlohmann/args.h"

#include <optional>

namespace tb
{

using namespace nlohmann;

bool
is_key(const std::string& str) {
  return str.length() > 1 && str.at(0) == '-';
}

bool
is_value(const std::optional<std::string>& next) {
  return next ? !is_key(next.value()) : false;
}

bool
is_short(const std::string& str) {
  return str.length() > 1
    && str.at(0) == '-'
    && str.at(1) != '-';
}

bool
is_long(const std::string& str) {
  return str.length() > 1
    && str.at(0) == '-'
    && str.at(1) == '-';
}

std::string
remove_dashes(const std::string& str) {
  if(is_short(str)) {
    return str.substr(1);
  }
  else if(is_long(str)) {
    return str.substr(2);
  }

  return str;
}

std::optional<std::string>
get_next(int argc, char** argv, int index) {
  return index + 1 < argc
                ? std::optional(argv[index+1])
                : std::nullopt;
}

std::string
char_at(const std::string& str, size_t index) {
  if(index < str.size()) {
    return std::string(1,str.at(index));
  }

  return "";
}

json
parse ( int argc, char** argv ) {
  json j;

  int position = 0;

  for(int index = 1; index < argc; index++) {
    std::string curr = argv[index];
    auto next = get_next(argc, argv, index);

    if(is_key(curr)) {
      if(is_long(curr)) {
        if(is_value(next)) {
          j[remove_dashes(curr)] = next.value();
          index++;
        }
        else{
          j[remove_dashes(curr)] = true;
        }
      }
      else{
        size_t c = 1;
        for(; c < curr.size() - 1; c++) {
          j[char_at(curr, c)] = true;
        }

        if(is_value(next)) {
          j[char_at(curr, c)] = next.value();
          index++;
        }
        else{
          j[char_at(curr, c)] = true;
        }
      }
    }
    else{
      j[std::to_string(position++)] = curr;
    }
  }

  return j;
} // parse

} // namespace tb
