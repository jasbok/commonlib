//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include <vector>

namespace tb {
template<typename T>
class circular_list {
	std::vector<T> items_;
	unsigned int current_ = 0;

public:
	circular_list() = default;

	circular_list(const std::vector<T>& items) : items_(items) {}

	virtual
	~circular_list() = default;

	T
	next() {
		if(++current_ >= items_.size()) {
			current_ = 0;
		}

		return items_.at(current_);
	}

	T
	previous() {
		if(--current_ > items_.size()) {
			current_ = items_.size() - 1;
		}

		return items_.at(current_);
	}

	T
	get() {
		return items_.at(current_);
	}

	auto
	cursor() {
		return current_;
	}

	auto
	begin() {
		return items_.begin();
	}

	auto
	end() {
		return items_.end();
	}

	auto
	size() {
		return items_.size();
	}

	auto&
	push_back(const T& item) {
		items_.push_back(item);

		return *this;
	}

	void
	remove_current() {
		if(items_.size() > 0) {
			items_.erase(items_.begin() + current_);
		}
	}
}; // class circular_list
} // namespace tb
