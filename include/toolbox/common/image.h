//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include "toolbox/common/coords.h"

#include <memory>
#include <string>
#include <vector>

namespace tb {

struct pixels {
  enum class format {
    greyscale,
    rg,
    rgb,
    rgba
  };

  enum class type {
    unsigned_byte
  };

  const tb::dims2<int> dims;
  const enum format format = format::rgb;
  const enum type type = type::unsigned_byte;
  const uint8_t *data;
};

struct image {
  std::unique_ptr<uint8_t[]> data;
  struct pixels pixels;
};

image
create_image(const struct pixels& pixels);

image
read_image(const std::string &path);

class read_image_exception : public std::exception {
  std::string mesg_;
public:
  read_image_exception(const std::string& mesg);

  const char *
  what() const throw();
};

} // namespace tb
