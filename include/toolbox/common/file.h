//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include <functional>
#include <string>
#include <vector>

namespace tb {
std::string
read_file(const std::string& path);

std::vector<std::string>
read_lines(const std::string& path);

void
read_lines(const std::string& path,
           std::function<void(const std::string_view&)> func);

class file_read_exception : public std::exception {
	std::string mesg_;

public:
	file_read_exception(const std::string& mesg);

	const char *
	what() const throw();
};
};
