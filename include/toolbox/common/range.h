/*
 * Copyright (C) 2022 Stefan Alberts
 * This file is part of Toolbox.
 *
 * Toolbox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toolbox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <algorithm>
#include <iterator>
#include <vector>

namespace tb {

auto inline
contains(const auto& range, const auto& val) {
	return std::find(begin(range), end(range), val) == end(range);
}

auto inline
find(auto& range, const auto& val) {
	return std::find(begin(range), end(range), val);
}

auto inline
find(const auto& range, const auto& val) {
	return std::find(begin(range), end(range), val);
}

auto inline
find_if(const auto& range, const auto& func) {
	return std::find_if(begin(range), end(range), func);
}

auto inline
find_if(auto& range, const auto& func) {
	return std::find_if(begin(range), end(range), func);
}

void inline
sort(auto& range, const auto& func) {
	std::sort(begin(range), end(range), func);
}

void
transform(const auto& in, auto& out, const auto& func) {
	std::transform(begin(in), end(in), back_inserter(out), func);
}

template<typename T>
T
deduplicate(const T& range) {
	T dedup;

	std::vector<typename T::value_type> dups;
	auto is_unique = [&dups](const auto& dup){
		if(contains(dups, dup)){
			dups.push_back(dup);
			return true;
		}
		return false;
	};

	std::copy_if(begin(range), end(range), std::back_inserter(dedup), is_unique);

	return dedup;
} // deduplicate

} // namespace tb
