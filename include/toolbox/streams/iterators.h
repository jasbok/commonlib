//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include "toolbox/streams/concepts.h"
#include <optional>

namespace tb::streams {

template<stream STREAM, counter_op<STREAM> COUNTER_OP>
class counter_terminal_iterator {
	STREAM stream_;
	COUNTER_OP op_;
	int counter_;

public:
	using output_t = typename STREAM::output_t;

	counter_terminal_iterator(const STREAM& stream, COUNTER_OP op)
		: stream_(stream), op_(op), counter_(0){}

	counter_terminal_iterator(STREAM&& stream, COUNTER_OP op)
		: stream_(std::forward<STREAM>(stream)), op_(op), counter_(0){}

	std::optional<output_t>
	next(){
		return op_(counter_++) ? stream_.next() : std::nullopt;
	}
};

template<stream STREAM, counter_op<STREAM> COUNTER_OP>
class counter_seek_iterator {
	STREAM stream_;
	COUNTER_OP op_;
	int counter_;

public:
	using output_t = typename STREAM::output_t;

	counter_seek_iterator(const STREAM& stream, COUNTER_OP op)
		: stream_(stream), op_(op), counter_(0){}

	counter_seek_iterator(STREAM&& stream, COUNTER_OP op)
		: stream_(std::forward<STREAM>(stream)), op_(op), counter_(0){}

	std::optional<output_t>
	next(){
		while(op_(counter_++) == false && stream_.next());
		return stream_.next();
	}
};

template<stream STREAM, item_op<STREAM> ITEM_OP>
class seek_iterator {
	STREAM stream_;
	ITEM_OP item_op_;

public:
	using output_t = typename STREAM::output_t;

	seek_iterator(const STREAM& stream, ITEM_OP item_op)
		: stream_(stream), item_op_(item_op){}

	seek_iterator(STREAM&& stream, ITEM_OP item_op)
		: stream_(std::forward<STREAM>(stream)), item_op_(item_op){}

	std::optional<output_t>
	next(){
		std::optional<output_t> it;
		while((it = stream_.next()) && !item_op_(it.value()));
		return it;
	}
};

template<stream STREAM, item_op<STREAM> ITEM_OP>
class collect_iterator {
	STREAM stream_;
	ITEM_OP item_op_;

public:
	using output_t = typename std::invoke_result<
		ITEM_OP, const typename STREAM::output_t&>::type::value_type;

	collect_iterator(const STREAM& stream, ITEM_OP item_op)
		: stream_(stream), item_op_(item_op){}

	collect_iterator(STREAM&& stream, ITEM_OP item_op)
		: stream_(std::forward<STREAM>(stream)), item_op_(item_op){}

	std::optional<output_t>
	next(){
		std::optional<typename STREAM::output_t> it;
		std::optional<output_t> ret;
		while((it = stream_.next()) && !(ret = item_op_(it.value())));
		return ret;
	}
};

template<stream STREAM, item_op<STREAM> ITEM_OP>
class stream_iterator {
	STREAM stream_;
	ITEM_OP item_op_;

public:
	using output_t =
		typename std::invoke_result<ITEM_OP, const typename STREAM::output_t&>::type;

	stream_iterator(const STREAM& stream, ITEM_OP item_op)
		: stream_(stream), item_op_(item_op){
	}

	stream_iterator(STREAM&& stream, ITEM_OP item_op)
		: stream_(std::forward<STREAM>(stream)), item_op_(item_op){
	}

	std::optional<output_t>
	next(){
		auto it = stream_.next();
		return it ? std::optional(item_op_(it.value())) : std::nullopt;
	}
};

template<stream STREAM>
class flatten_iterator {
	STREAM stream_;

	using inner_t = typename STREAM::output_t;
	std::optional<inner_t> inner_;

public:
	using output_t = typename inner_t::output_t;

	// TODO: Why is this broken? Is there a bug else where?
	//       Is the lazy version broken, cancelling out a bug in the iterators?
	// flatten_iterator(const STREAM& stream)
	// 	: stream_(stream), inner_(stream_.next()){}

	// flatten_iterator(STREAM&& stream)
	// 	: stream_(std::forward<STREAM>(stream)), inner_(stream_.next()){}

	// std::optional<output_t>
	// next(){
	// 	while(inner_){
	// 		if(auto it = inner_->next()){ return it; }
	// 		inner_ = stream_.next();
	// 	}
	// 	return std::nullopt;
	// }

	flatten_iterator(const STREAM& stream) : stream_(stream), inner_(){}

	flatten_iterator(STREAM&& stream)
		: stream_(std::forward<STREAM>(stream)), inner_(){}

	std::optional<output_t>
	next(){
		while(inner_){
			if(auto it = inner_->next()){ return it;}
			inner_ = stream_.next();
		}
		return (inner_ = stream_.next()) ? next() : std::nullopt;
	}
};

template<stream STREAM, typename RESULT, recurrent_op<STREAM, RESULT> ITEM_OP>
class recurrent_iterator {
	STREAM stream_;
	RESULT prev_;
	ITEM_OP item_op_;

public:
	using output_t = RESULT;

	recurrent_iterator(const STREAM& stream, const RESULT& start, ITEM_OP item_op)
		: stream_(stream), prev_(start), item_op_(item_op){}

	recurrent_iterator(STREAM&& stream, const RESULT& start, ITEM_OP item_op)
		: stream_(std::forward<STREAM>(stream)), prev_(start), item_op_(item_op){}

	std::optional<output_t>
	next(){
		if(auto it = stream_.next()){
			prev_ = item_op_(prev_, it.value());
			return std::optional(prev_);
		}
		return std::nullopt;
	}
};

template<stream STREAM, item_op<STREAM> ITEM_OP>
class peek_iterator {
	STREAM stream_;
	ITEM_OP item_op_;

public:
	using output_t = typename STREAM::output_t;

	peek_iterator(const STREAM& stream, ITEM_OP item_op)
		: stream_(stream), item_op_(item_op){
	}

	peek_iterator(STREAM&& stream, ITEM_OP item_op)
		: stream_(std::forward<STREAM>(stream)), item_op_(item_op){
	}

	std::optional<output_t>
	next(){
		auto it = stream_.next();
		if(it){ item_op_(it.value()); }
		return it;
	}
};

template<stream STREAM_HEAD, stream STREAM_TAIL>
class concat_iterator {
	STREAM_HEAD head_;
	STREAM_TAIL tail_;

public:
	using output_t = typename STREAM_HEAD::output_t;

	concat_iterator(const STREAM_HEAD& head, const STREAM_TAIL& tail)
		: head_(head), tail_(tail){}

	concat_iterator(STREAM_HEAD&& head, STREAM_TAIL&& tail)
		: head_(std::forward<STREAM_HEAD>(head)),
		  tail_(std::forward<STREAM_TAIL>(tail)){}

	std::optional<output_t>
	next(){
		auto it = head_.next();
		return it ? it : tail_.next();
	}
};

} // namespace tb::streams
