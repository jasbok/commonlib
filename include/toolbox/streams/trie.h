//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include "toolbox/streams/collectors.h"
#include "toolbox/streams/generators.h"
#include "toolbox/streams/operators.h"

#include <functional>
#include <unordered_map>

namespace tb::streams::trie {

using node_id = std::size_t;

template<typename T>
struct node {
	T val;
	std::vector<node_id> children;
};

template<typename T>
using trie = std::vector<node<T>>;

template<typename T>
node_id
traverse(const trie<T>& trie, node_id start, const T& val){
	for(auto next : trie[start].children){
		if(trie[next].val == val){
			return next;
		}
	}

	return trie.size();
}

template<iterable IT>
node_id
traverse(const trie<typename IT::value_type>& trie, const IT& collection){
	node_id curr = 0;
	for(auto it : collection){
		curr = traverse(trie, curr, it);
		if(curr == trie.size()){ return 0; };
	}
	return curr;
}

inline node_id
traverse(const trie<char>& trie, const char* collection){
	return traverse(trie, std::string(collection));
}

template<typename T>
node_id
add(trie<T>& trie, node_id start, const T& val){
	auto next = traverse(trie, start, val);

	if(next == trie.size()){
		trie[start].children.push_back(next);
		trie.push_back({.val = val});
	}

	return next;
}

template<typename T, iterable IT>
void
add(trie<T>& trie, const IT& collection){
	node_id curr = 0;
	for(auto it : collection){
		curr = add(trie, curr, it);
	}
}

template<iterable IT>
auto
from(const std::vector<IT>& items){
	auto tr = trie<typename IT::value_type>{{}};
	for(auto it : items){ add(tr, it); }
	return tr;
}

}; // namespace tb::streams::sm
