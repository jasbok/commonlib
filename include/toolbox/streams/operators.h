//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include "toolbox/streams/iterators.h"

namespace tb::streams {

class limit {
	int n_;

public:
	limit(int n) : n_(n){}

	auto
	connect(auto stream){
		return counter_terminal_iterator(std::move(stream),
		                                 [n = n_](auto i){ return i < n; });
	}
};

class skip {
	int n_;

public:
	skip(int n) : n_(n){}

	auto
	connect(auto stream){
		return counter_seek_iterator(std::move(stream),
		                             [n = n_-1](auto i){ return n < i; });
	}
};

template <typename FUNC>
class filter {
	FUNC func_;

public:
	filter(FUNC func) : func_(func) {}

	auto
	connect(auto stream){
		return seek_iterator(std::move(stream), func_);
	}
};

template <typename FUNC>
class map {
	FUNC func_;

public:
	map(FUNC func) : func_(func) {}

	auto
	connect(auto stream){
		return stream_iterator(std::move(stream), func_);
	}
};

class flatten {
public:
	auto
	connect(auto stream){
		return flatten_iterator(std::move(stream));
	}
};

template <typename FUNC>
class flat_map {
	FUNC func_;

public:
	flat_map(FUNC func) : func_(func) {}

	auto
	connect(auto stream){
		return stream | flatten() | map(func_);
	}
};

template <typename FUNC>
class peek {
	FUNC func_;

public:
	peek(FUNC func) : func_(func) {}

	auto
	connect(auto stream){
		return peek_iterator(std::move(stream), func_);
	}
};

template <typename FUNC>
class collect {
	FUNC func_;

public:
	collect(FUNC func) : func_(func) {}

	auto
	connect(auto stream){
		return collect_iterator(std::move(stream), func_);
	}
};

template <typename RET, typename FUNC>
class recurrent {
	RET start_;
	FUNC func_;

public:
	recurrent(RET start, FUNC func) : start_(start), func_(func) {}

	auto
	connect(auto stream){
		return recurrent_iterator(std::move(stream), start_, func_);
	}
};

template <stream STREAM_RH>
class zip {
	STREAM_RH rh_;

public:
	zip(const STREAM_RH& rh) : rh_(rh){}
	zip(STREAM_RH&& rh) : rh_(std::forward<STREAM_RH>(rh)){}

	auto
	connect(auto lh){
		return stream_iterator(std::move(lh), [&rh = rh_](auto val_lh){
			auto it_rh = rh.next();
			return std::pair(val_lh,
			                 it_rh ? it_rh.value() : typename STREAM_RH::output_t());
		});
	}
};

template<stream STREAM_RH>
class concat {
	STREAM_RH rh_;

public:
	concat(const STREAM_RH& rh) : rh_(rh){}
	concat(STREAM_RH&& rh) : rh_(std::forward<STREAM_RH>(rh)){}

	auto
	connect(auto lh){
		return concat_iterator(std::move(lh), std::move(rh_));
	}
};

template<stream LH, typename RH>
auto
operator|(LH&& lh, RH&& rh){
	return rh.connect(std::forward<LH>(lh));
}

template<iterable LH, typename RH>
auto
operator|(LH&& lh, RH&& rh){
	return rh.connect(iterator(std::forward<LH>(lh)));
}

} // namespace tb::streams
