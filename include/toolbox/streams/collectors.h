//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include "toolbox/streams/concepts.h"
#include "toolbox/streams/generators.h"

#include <algorithm>
#include <unordered_map>
#include <vector>

namespace tb::streams {

class to_vec {
public:
	auto
	connect(auto stream){
		std::vector<typename decltype(stream)::output_t> vec;
		while(auto it = stream.next()){ vec.push_back(it.value()); }
		return vec;
	}
};

class to_string {
public:
	auto
	connect(auto stream){
		std::string str;
		while(auto i = stream.next()){ str.push_back(i.value()); }
		return str;
	}
};

template <typename ACC, typename FUNC>
class fold {
	ACC acc_;
	FUNC func_;

public:
	fold(ACC start, FUNC func) : acc_(start), func_(func){}

	fold(const fold& copy) = delete;
	fold(fold&& move) = delete;
	fold& operator=(const fold& copy) = delete;
	fold& operator=(fold&& move) = delete;

	template<typename STREAM>
	ACC
	connect(STREAM stream){
		while(auto it = stream.next()){ acc_ = func_(acc_, it.value()); }
		return acc_;
	}
};

class head {
	int n_;

public:
	head(int n) : n_(n){}

	auto
	connect(auto stream){
		using vec = std::vector<typename decltype(stream)::output_t>;
		if(n_ <= 0) return vec{};

		int index = 0; vec state(n_);

		for(auto it = stream.next(); it && index < n_; it = stream.next(), index++){
			state[index] = it.value();
		}

		if(index <= n_){ state.resize(index); }
		return state;
	}
};

class first {
public:
	auto
	connect(auto stream){
		return stream.next();
	}
};

class tail {
	int n_;

public:
	tail(int n) : n_(n){}

	auto
	connect(auto stream){
		using vec = std::vector<typename decltype(stream)::output_t>;
		if(n_ <= 0) return vec{};

		int index = 0; vec state(n_);

		for(auto it = stream.next(); it; it = stream.next(), index++){
			state[index % n_] = it.value();
		}

		if(index <= n_){ state.resize(index); return state; }

		vec ret; ret.reserve(n_); int start = index % n_;
		std::copy(state.begin() + start, state.end(), std::back_inserter(ret));
		std::copy(state.begin(), state.begin() + start, std::back_inserter(ret));

		return ret;
	}
};

class last {
public:
	auto
	connect(auto stream){
		decltype(stream.next()) last;
		while(auto it = stream.next()){ last = it; }
		return last;
	}
};

template<typename FUNC>
class sort {
	FUNC func_;

public:
	sort(FUNC func) : func_(func){}

	auto
	connect(auto stream){
		auto vec = stream | to_vec();
		std::sort(vec.begin(), vec.end(), func_);
		return vec;
	}
};

class reverse {
public:
	auto
	connect(auto stream){
		auto vec = stream | to_vec();
		std::reverse(vec.begin(), vec.end());
		return vec;
	}
};

class histogram {
public:
	auto
	connect(auto stream){
		using key_t = typename decltype(stream)::output_t;
		std::unordered_map<key_t, int> map;
		while(auto i = stream.next()){
			auto val = i.value();

			if(map.find(val) == map.end()){ map[val] = 0; }
			map[val] += 1;
		}

		std::vector<std::pair<key_t, int>> histo(map.size());
		std::copy(map.begin(), map.end(), histo.begin());
		return histo;
	}
};

template<typename FUNC>
class group_by {
	FUNC func_;
public:
	group_by(FUNC func) : func_(func) {}

	auto
	connect(auto stream){
		using stream_t = typename decltype(stream)::output_t;
		using key_t = typename std::invoke_result<FUNC, const stream_t&>::type;
		std::unordered_map<key_t, std::vector<stream_t>> map;

		while(auto i = stream.next()){
			auto val = i.value();
			auto key = func_(val);

			if(map.find(key) == map.end()){ map[key] = {}; }
			map[key].push_back(val);
		}

		return map;
	}
};

}; // namespace tb::streams
