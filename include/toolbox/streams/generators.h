//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include <fstream>
#include <initializer_list>
#include <optional>
#include <string>
#include <vector>

namespace tb::streams {

template<typename T>
class range {
	T start_, end_, step_, curr_;

public:
	using output_t = T;

	range(T start, T end, T step)
		: start_(start), end_(end), step_(step), curr_(start_){}

	range(T start, T end)
		: start_(start), end_(end), step_(start_ < end_ ? 1 : -1), curr_(start_){}

	range(T end)
		: start_(0), end_(end), step_(start_ < end_ ? 1 : -1), curr_(start_){}

	range(const range& copy) :
		start_(copy.start_), end_(copy.end_), step_(copy.step_), curr_(start_){
		// printf("range_generator - copy construct\n");
	}

	range(range&& move) :
		// Might have to change to curr_(move.curr_)
		start_(move.start_), end_(move.end_), step_(move.step_), curr_(start_){
		// printf("range_generator - move construct\n");
	}

	range&
	operator=(const range& copy){
		// printf("range_generator - copy assignment\n");
		start_ = copy._start; end_ = copy._end; step_ = copy._end; curr_ = start_;
		return *this;
	}

	range&
	operator=(range&& move){
		// printf("range_generator - move assignment\n");
		// Might have to change to curr_ = move.curr_
		start_ = move._start; end_ = move._end; step_ = move._end; curr_ = start_;
		return *this;
	}

	virtual ~range() = default;

	std::optional<output_t>
	next(){
		auto it = step_ < 0
    ? (curr_ > end_ ? std::optional(curr_) : std::nullopt)
		    : (curr_ < end_ ? std::optional(curr_) : std::nullopt);
		curr_ += step_;
		return it;
	}
}; // class range_generator

template<typename T>
class iterator_ {
	T container_;
	typename T::iterator iter_;

public:
	using output_t = typename T::value_type;

	iterator_(const T& container)
		: container_(container), iter_(container_.begin()){}

	iterator_(T&& container)
		: container_(std::forward<T>(container)), iter_(container_.begin()){}

	iterator_(const iterator_& copy)
		: container_(copy.container_), iter_(container_.begin()){}

	iterator_(iterator_&& move)
		: container_(std::forward<T>(move.container_)), iter_(container_.begin()){}

	iterator_&
	operator=(const iterator_& copy){
		container_ = copy.container_;
		iter_ = container_.begin();
		return *this;
	}

	iterator_&
	operator=(iterator_&& move){
		container_ = std::forward<T>(move.container_);
		iter_ = container_.begin();
		return *this;
	}

	std::optional<output_t>
	next(){
		return iter_ == container_.end() ? std::nullopt : std::optional(*iter_++);
	}
};

template<typename T>
auto
iterator(const T& container){ return iterator_(container);}

template<typename T>
auto
iterator(T&& container){ return iterator_(std::forward<T>(container));}

template<typename I>
auto
iterator(std::initializer_list<I> init){ return iterator_(std::vector(init));}

template<typename CH>
auto
iterator(const CH* c_str){ return iterator_(std::string(c_str));}

template<typename CH = char>
class file {
	std::ifstream ifs_;
	std::istreambuf_iterator<CH> it_, end_;

public:
	using output_t = CH;

	file(const std::string& path) : ifs_(path), it_(ifs_), end_() {
		if(!ifs_.is_open()){
			throw std::runtime_error(std::string("Failed to open file: ") + path);
		}
	}

	file(file&& move) : ifs_(std::move(move.ifs_)), it_(ifs_), end_(){};

	file() = delete;
	file(const file& copy) = delete;
	file& operator=(const file& copy) = delete;
	file& operator=(file&& move) = delete;

	std::optional<CH>
	next(){
		return it_ == end_ ? std::nullopt : std::optional(*it_++);
	}
};

template<typename T>
class repeat {
	T val_;

public:
	using output_t = T;

	repeat(const T& val) : val_(val) {}

	std::optional<output_t>
	next(){
		return std::optional(val_);
	}
};

template<typename T>
class cycle {
	std::vector<T> values_;
	int counter_;

public:
	using output_t = T;

	cycle(const std::vector<T>& values) : values_(values), counter_(0) {}

	std::optional<output_t>
	next(){
		return std::optional(values_[counter_++ % values_.size()]);
	}
};

template<typename T>
class empty {
public:
	using output_t = T;
	std::optional<output_t>
	next(){ return std::nullopt; }
};

} // namespace tb::streams
