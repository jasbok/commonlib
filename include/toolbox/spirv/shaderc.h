//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include <shaderc/shaderc.hpp>

#include <string>

namespace tb {

class shaderc {
	std::string working_dir_;

public:
	shaderc(const std::string& working_dir);

	std::string
	preprocess_shader(shaderc_shader_kind kind,
	                  const std::string& name,
	                  const std::string& source);

	std::vector<uint32_t>
	compile_file(shaderc_shader_kind kind,
	             const std::string& name,
	             const std::string& source,
	             bool optimize = false);
};

}
